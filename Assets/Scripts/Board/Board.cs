﻿/**
 * @file Board.cs
 * @author naokinakagawa
 * @date 2015/11/09
 */

using System.IO;
using System.Text;
using System.Xml.Serialization;
using UnityEngine;

namespace naokinakagawa.ShogiMaker
{
	/**
	 * @brief 盤クラス
	 */
	public class Board : MonoBehaviour
	{
		private static RectTransform instance = null;	//!< インスタンスを保存する(GameObjectよりTransformのほうが便利)
		private static Grid size = new Grid();		//!< 盤のサイズ

		/**
		 * @brief 升のサイズ
		 */
		public static float gridSize
		{
			get;
			private set;
		}

		/**
		 * @brief 盤のサイズ
		 */
		public static Grid Size
		{
			get
			{
				return size;
			}
			set
			{
				size = value;

				// 今ある升オブジェクトをすべて消す
				foreach (Transform child in instance)
				{
					Destroy(child.gameObject);
				}

				// 升のサイズを計算
				if (CanvasAdapter.rectTransform.sizeDelta.x > CanvasAdapter.rectTransform.sizeDelta.y)
				{
					// 0.5は余白
					gridSize = CanvasAdapter.rectTransform.sizeDelta.y / (Size.row + 0.5f);
				}
				else
				{
					// 0.5は余白
					gridSize = CanvasAdapter.rectTransform.sizeDelta.x / (Size.col + 0.5f);
				}

				// 盤のサイズを設定
				instance.sizeDelta = new Vector2(gridSize * (Size.col + 0.5f), gridSize * (Size.row + 0.5f));

				// 升を並べる
				for (int y = 0; y < Size.row; ++y)
				{
					for (int x = 0; x < Size.col; ++x)
					{
						// 升を生成する
						RectTransform grid = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/Grid")).GetComponent<RectTransform>();

						// 盤の子オブジェクトにする
						grid.SetParent(instance);

						// 移動させる
						grid.localPosition = new Grid(y, x, -1f).ToVector3();

						// 大きさを変更する
						grid.sizeDelta = new Vector2(gridSize, gridSize);

						// 偶数なら偶数の奇数なら奇数のスプライトをアタッチする
						if ((y + x) % 2 == 0)
						{
							grid.gameObject.AddComponent<SkinGridEven>();
						}
						else
						{
							grid.gameObject.AddComponent<SkinGridOdd>();
						}
					}
				}
			}
		}

		/**
		 * @brief 初期化
		 */
		protected void Awake()
		{
			instance = GetComponent<RectTransform>();
		}
	}
}
