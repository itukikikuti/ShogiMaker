﻿/**
 * @file Grid.cs
 * @author naokinakagawa
 * @date 2015/11/09
 */

using UnityEngine;

namespace naokinakagawa.ShogiMaker
{
	/**
	 * @brief 升構造体
	 */
	[System.Serializable]
	public struct Grid
	{
		/**
		 * @brief コンストラクタ
		 */
		public Grid(int row, int col ,float z = 0f)
		{
			this.row = row;
			this.col = col;
			this.z = z;
		}

		/**
		 * @brief コンストラクタ
		 */
		public Grid(Vector3 vec)
		{
			// 原点(右上)の座標を計算する
			Vector3 origin = (new Vector2(Board.Size.col / 2f, Board.Size.row / 2f) * Board.gridSize) - (new Vector2(Board.gridSize / 2f, Board.gridSize / 2f));

			vec = (-vec + origin + (Vector3)(CanvasAdapter.rectTransform.sizeDelta / 2f)) / Board.gridSize;

			this = new Grid(Mathf.RoundToInt(vec.y), Mathf.RoundToInt(vec.x), vec.z);
		}

		public int row;	//!< 行(将棋で言う段)
		public int col;	//!< 列(将棋で言う筋)
		public float z;	//!< Z座標

		/**
		 * @brief 等しいかどうかチェックする
		 * @param obj チェックするオブジェクト
		 */
		public override bool Equals(object obj)
		{
			if (obj == null || this.GetType() != obj.GetType())
			{
				return false;
			}

			return (this == (Grid)obj);
		}

		/**
		 * @brief ハッシュコードを取得する
		 */
		public override int GetHashCode()
		{
			return this.row ^ this.col;
		}

		/**
		 * @brief 文字列として出力する
		 */
		public override string ToString()
		{
			return "(" + row.ToString() + ", " + col.ToString() + ")";
		}

		/**
		 * @brief 等しいかどうか調べる
		 * @param _1 1個目のオブジェクト
		 * @param _2 2個目のオブジェクト
		 */
		public static bool operator == (Grid _1, Grid _2)
		{
			return (_1.row == _2.row && _1.col == _2.col);
		}

		/**
		 * @brief 等しくないかどうか調べる
		 * @param _1 1個目のオブジェクト
		 * @param _2 2個目のオブジェクト
		 */
		public static bool operator != (Grid _1, Grid _2)
		{
			return (_1.row != _2.row || _1.col != _2.col);
		}

		/**
		 * @brief GridからVector3への変換式
		 */
		public Vector3 ToVector3()
		{
			// 原点(右上)の座標を計算する
			Vector3 origin = (new Vector2(Board.Size.col / 2f, Board.Size.row / 2f) * Board.gridSize) - (new Vector2(Board.gridSize / 2f, Board.gridSize / 2f));

			return origin - new Vector3(this.col * Board.gridSize, this.row * Board.gridSize, this.z);
		}
	}
}
