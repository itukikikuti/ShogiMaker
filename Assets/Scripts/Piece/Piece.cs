﻿using UnityEngine;
using UnityEngine.UI;

namespace naokinakagawa.ShogiMaker
{
	public class Piece : MonoBehaviour
	{
		/**
		 * @brief 駒を作成する
		 */
		public static Piece Build(Grid grid)
		{
			// 駒を生成する
			GameObject piece = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/Piece"));

			// キャンバスを親オブジェクトにする
			piece.GetComponent<RectTransform>().SetParent(CanvasAdapter.rectTransform);

			// 駒を移動する
			piece.GetComponent<Piece>().grid = grid;

			// 大きさを変更する
			piece.GetComponent<RectTransform>().sizeDelta = new Vector2(Board.gridSize, Board.gridSize);

			piece.GetComponentInChildren<Text>().fontSize = (int)(Board.gridSize * 0.75f);

			return piece.GetComponent<Piece>();
		}

		/**
		 * @brief スクリーン座標
		 */
		public RectTransform rectTransform
		{
			get
			{
				return GetComponent<RectTransform>();
			}
		}

		/**
		 * @brief スクリーン座標
		 */
		public Vector3 position
		{
			get
			{
				return GetComponent<RectTransform>().position;
			}
			set
			{
				GetComponent<RectTransform>().position = value;
			}
		}

		/**
		 * @brief 升
		 */
		public Grid grid
		{
			get
			{
				return new Grid(GetComponent<RectTransform>().localPosition);
			}
			set
			{
				GetComponent<RectTransform>().localPosition = value.ToVector3();
			}
		}
	}
}
