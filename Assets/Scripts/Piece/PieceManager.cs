﻿using UnityEngine;

namespace naokinakagawa.ShogiMaker
{
	public class PieceManager : MonoBehaviour
	{
		public string skinName;
		public Grid boardSize;

		private Piece[,] pieces;

		private HoldPiece holdPiece = null;

		/**
		 * @brief 初期化
		 */
		protected void Start()
		{
			Skin.Name = skinName;

			Board.Size = boardSize;

			pieces = new Piece[Board.Size.row, Board.Size.col];

			for (int i = 0; i < Board.Size.col; ++i)
			{
				pieces[8, i] = Piece.Build(new Grid(8, i));
			}
		}

		protected void Update()
		{
			if (Input.GetMouseButtonDown(0) && holdPiece == null)
			{
				Debug.Log("a");
				Grid mouseGrid = new Grid(Input.mousePosition);

				if (mouseGrid.row >= 0 && mouseGrid.row < Board.Size.row &&
				    mouseGrid.col >= 0 && mouseGrid.col < Board.Size.col)
				{
					if (pieces[mouseGrid.row, mouseGrid.col] != null)
					{
						holdPiece = new HoldPiece();

						holdPiece.grid = mouseGrid;
					}
				}
			}

			if (Input.GetMouseButton(0) && holdPiece != null)
			{
				Debug.Log("b");
				pieces[holdPiece.grid.row, holdPiece.grid.col].position = Input.mousePosition + new Vector3(-Board.gridSize / 2f, Board.gridSize / 2f);

				holdPiece.index = pieces[holdPiece.grid.row, holdPiece.grid.col].rectTransform.GetSiblingIndex();
				pieces[holdPiece.grid.row, holdPiece.grid.col].rectTransform.SetAsLastSibling();
			}

			if (Input.GetMouseButtonUp(0) && holdPiece != null)
			{
				Debug.Log("c");
				Grid mouseGrid = new Grid(Input.mousePosition);
				pieces[holdPiece.grid.row, holdPiece.grid.col].grid = mouseGrid;
				pieces[holdPiece.grid.row, holdPiece.grid.col].rectTransform.SetSiblingIndex(holdPiece.index);

				if (holdPiece.grid != mouseGrid)
				{
					pieces[mouseGrid.row, mouseGrid.col] = pieces[holdPiece.grid.row, holdPiece.grid.col];
					pieces[holdPiece.grid.row, holdPiece.grid.col] = null;
				}

				holdPiece = null;
			}
		}

		protected class HoldPiece
		{
			public Grid grid;
			public int index;
		}
	}
}
