﻿/**
 * @file Skin.cs
 * @author naokinakagawa
 * @date 2015/11/08
 */

using System;
using UnityEngine;

namespace naokinakagawa.ShogiMaker
{
	/**
	 * @brief スキンを設定するクラス
	 */
	public static class Skin
	{
		/**
		 * @brief 盤のスプライト
		 */
		public static Sprite boardSprite
		{
			get;
			private set;
		}

		/**
		 * @brief 升のスプライト
		 */
		public static Sprite gridEvenSprite
		{
			get;
			private set;
		}

		/**
		 * @brief 升のスプライト
		 */
		public static Sprite gridOddSprite
		{
			get;
			private set;
		}

		/**
		 * @brief 駒のスプライト
		 */
		public static Sprite pieceSprite
		{
			get;
			private set;
		}

		/**
		 * @brief フォント
		 */
		public static Font font
		{
			get;
			private set;
		}

		private static string name;	//!< スキンの名前

		/**
		 * @brief スキンの名前
		 */
		public static string Name
		{
			get
			{
				return name;
			}
			set
			{
				name = value;

				// スプライトを変更する
				boardSprite = Resources.Load<Sprite>("Skins/" + value + "/Board");
				gridEvenSprite = Resources.Load<Sprite>("Skins/" + value + "/GridEven");
				gridOddSprite = Resources.Load<Sprite>("Skins/" + value + "/GridOdd");
				pieceSprite = Resources.Load<Sprite>("Skins/" + value + "/Piece");
				font = Resources.Load<Font>("Skins/" + value + "/Font");

				ChangeSkinEvent ();
			}
		}

		/**
		 * @brief ChangeSkinが呼ばれた時のイベント
		 */
		public static event Action ChangeSkinEvent = ()=>{};

		/**
		 * @brief スキンを適用するオブジェクトにアタッチするスクリプトのスーパークラス
		 */
		public abstract class SkinInterface : MonoBehaviour
		{
			/**
			 * @brief 初期化
			 */
			protected void Awake()
			{
				ChangeSkinEvent += OnChangeSkin;
			}

			/**
			 * @brief スキンが変更されたときの処理
			 */
			protected abstract void OnChangeSkin();

			/**
			 * @brief 破棄
			 */
			public void OnDestroy()
			{
				ChangeSkinEvent -= OnChangeSkin;
			}
		}
	}
}
