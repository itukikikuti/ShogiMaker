﻿/**
 * @file SkinGridEven.cs
 * @author naokinakagawa
 * @date 2015/11/08
 */

using UnityEngine;
using UnityEngine.UI;

namespace naokinakagawa.ShogiMaker
{
	/**
	 * @brief 升のスプライトを変更する
	 */
	public class SkinGridEven : Skin.SkinInterface
	{
		/**
		 * @brief 初期化
		 */
		protected void Start()
		{
			OnChangeSkin();
		}

		/**
		 * @brief スキンが変更されたときの処理
		 */
		protected override void OnChangeSkin()
		{
			GetComponent<Image>().sprite = Skin.gridEvenSprite;
		}
	}
}
