﻿/**
 * @file CanvasAdapter.cs
 * @author naokinakagawa
 * @date 2015/11/09
 */

using UnityEngine;

namespace naokinakagawa.ShogiMaker
{
	/**
	 * @brief Canvasの状態を取得するクラス
	 */
	public class CanvasAdapter : MonoBehaviour
	{
		private static RectTransform instance = null;	//!< インスタンスを保存する(GameObjectよりTransformのほうが便利)

		/**
		 * @brief キャンバスを取得する
		 */
		public static Canvas canvas
		{
			get
			{
				return instance.GetComponent<Canvas>();
			}
		}

		/**
		 * @brief サイズを取得する
		 */
		public static RectTransform rectTransform
		{
			get
			{
				return instance;
			}
		}

		/**
		 * @brief 初期化
		 */
		protected void Awake()
		{
			instance = GetComponent<RectTransform>();
		}
	}
}
